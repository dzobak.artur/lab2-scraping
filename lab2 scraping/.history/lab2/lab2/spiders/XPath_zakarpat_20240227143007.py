import scrapy
from lab2.items import SanatoryItem, DescriptionItem


class ZakarpatSpider(scrapy.Spider):
    name = "zakarpat"
    allowed_domains = ["zakarpattyachko.com.ua"]
    start_urls = ["https://zakarpattyachko.com.ua/"]

    def parse(self, response):
        sanat_list = response.xpath('//ul[@class="dropdown-menu sub-menu"]')
        if sanat_list:
            for li in sanat_list.xpath('.//li'):
                a = li.xpath('.//a')
                span = a.xpath('.//span')

                sanat_name = span.xpath('string()').get()
                sanat_url = response.urljoin(a.attrib['href'])
                yield SanatoryItem(
                    name=sanat_name,
                    url=sanat_url
                )
                yield scrapy.Request(
                    url=sanat_url,
                    callback=self.parse_sanat,
                    meta={"sanat": sanat_name}
                )

    def parse_sanat(self, response):
        comment_boxes = response.xpath('//*[@class="comment-box"]')

        for comment_box in comment_boxes:
            comment_text = comment_box.xpath('.//div[@class="comment-body"]/text()').get().strip()
            
            yield DescriptionItem(
                name=comment_text
            )
