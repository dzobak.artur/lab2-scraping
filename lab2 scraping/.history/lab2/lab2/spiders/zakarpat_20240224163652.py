import scrapy
from bs4 import BeautifulSoup
from lab2.items import SanatoryItem, DescriptionItem


class ZakarpatSpider(scrapy.Spider):
    name = "zakarpat"
    allowed_domains = ["zakarpattyachko.com.ua"]
    start_urls = ["https://zakarpattyachko.com.ua"]

    def parse(self, response):
        soup = BeautifulSoup(response.body,  "html.parser")
        
        fac_list = soup.find(class_="nav nav-list footer-list")
        for li in fac_list.find_all("li"):
            a = li.find("a")
            
            sanat_name = a.find(string=True, recursive=False)
            sanat_url = f"https://zakarpattyachko.com.ua{a.get('href')}"
            yield SanatoryItem(
                name=sanat_name,
                url=sanat_url
            )

    
        
