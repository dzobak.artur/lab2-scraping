import scrapy
from bs4 import BeautifulSoup
from lab2.items import SanatoryItem, DescriptionItem


class ZakarpatSpider(scrapy.Spider):
    name = "zakarpat"
    allowed_domains = ["zakarpattyachko.com.ua"]
    start_urls = ["https://zakarpattyachko.com.ua"]

    def parse(self, response):
        soup = BeautifulSoup(response.body,  "html.parser")
        # знаходимо список факультетів і для кожного факультету
        fac_list = soup.find(class_="departments_unfolded")
        for li in fac_list.find_all("li"):
            a = li.find("a")
            # в <a> знаходимо ім'я і посилання на сторінку факультету
            fac_name = a.find(string=True, recursive=False)
            fac_url = f"https://uzhnu.edu.ua{a.get('href')}"

    
        
