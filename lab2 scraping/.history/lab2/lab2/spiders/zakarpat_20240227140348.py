import scrapy
from bs4 import BeautifulSoup
from lab2.items import SanatoryItem, DescriptionItem


class ZakarpatSpider(scrapy.Spider):
    name = "zakarpat"
    allowed_domains = ["zakarpattyachko.com.ua"]
    start_urls = ["https://zakarpattyachko.com.ua/"]

    def parse(self, response):
        soup = BeautifulSoup(response.text, "html.parser")

        sanat_list = soup.find(class_="dropdown-menu sub-menu")
        if sanat_list:
            for li in sanat_list.find_all("li"):
                a = li.find("a")
                span = a.find("span")

                sanat_name = span.find(text=True, recursive=False)
                sanat_url = f"https://zakarpattyachko.com.ua/{a.get('href')}"
                yield SanatoryItem(
                    name=sanat_name,
                    url=sanat_url
                )
                yield scrapy.Request(
                    url=sanat_url,
                    callback=self.parse_sanat,
                    meta={
                        "sanat": sanat_name
                    }
                )

    def parse_sanat(self, response):
        
        soup = BeautifulSoup(response.text, "html.parser")
        
        dep_list = soup.find(id_="comment-body-text")

        if dep_list:
            for li in dep_list.find_all("li"):
                #a = div.find("a")
                #span = a.find("span")
                dep_name = li.find(text=True, recursive=False)
                #dep_url = f"https://zakarpattyachko.com.ua/{a.get('href')}"
                yield DescriptionItem(
                    
                    name=dep_name,
                    #url=dep_url,
                )
