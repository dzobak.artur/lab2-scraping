import scrapy
from bs4 import BeautifulSoup
from lab2.items import SanatoryItem, DescriptionItem


class ZakarpatSpider(scrapy.Spider):
    name = "zakarpat"
    allowed_domains = ["zakarpattyachko.com.ua"]
    start_urls = ["https://zakarpattyachko.com.ua/"]

    def parse(self, response):
        soup = BeautifulSoup(response.text, "html.parser")

        sanat_list = soup.find(class_="dropdown-menu sub-menu")
        if sanat_list:
            for li in sanat_list.find_all("li"):
                a = li.find("a")
                span = a.find("span")

                sanat_name = span.find(text=True, recursive=False)
                sanat_url = f"https://zakarpattyachko.com.ua/{a.get('href')}"
                yield SanatoryItem(
                    name=sanat_name,
                    url=sanat_url
                )
                yield scrapy.Request(
                    url=sanat_url,
                    callback=self.parse_sanat,
                    meta={
                        "sanat": sanat_name
                    }
                )

    def parse_sanat(self, response):
        soup = BeautifulSoup(response.text, "html.parser")

    # Find all elements with class "comment-box"
        comment_boxes = soup.find_all(class_="comment-box")

    # Extract the comment text from each comment box
    for comment_box in comment_boxes:
        # Find the element containing the comment text
        comment_text_element = comment_box.find(class_="comment-body")

        # If the comment text element is found, extract the text
        if comment_text_element:
            comment_text = comment_text_element.get_text(strip=True)
            yield DescriptionItem(name=comment_text)
