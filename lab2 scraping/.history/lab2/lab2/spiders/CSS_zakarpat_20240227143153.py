import scrapy
from lab2.items import SanatoryItem, DescriptionItem


class CSSzakarpatSpider(scrapy.Spider):
    name = "zakarpat"
    allowed_domains = ["zakarpattyachko.com.ua"]
    start_urls = ["https://zakarpattyachko.com.ua/"]

    def parse(self, response):
        sanat_list = response.css('.dropdown-menu.sub-menu')
        if sanat_list:
            for li in sanat_list.css('li'):
                a = li.css('a')
                span = a.css('span')

                sanat_name = span.css('::text').get()
                sanat_url = response.urljoin(a.attrib['href'])
                yield SanatoryItem(
                    name=sanat_name,
                    url=sanat_url
                )
                yield scrapy.Request(
                    url=sanat_url,
                    callback=self.parse_sanat,
                    meta={"sanat": sanat_name}
                )

    def parse_sanat(self, response):
        comment_boxes = response.css('.comment-box')

        for comment_box in comment_boxes:
            comment_text = comment_box.css('.comment-body::text').get().strip()
            
            yield DescriptionItem(
                name=comment_text
            )
