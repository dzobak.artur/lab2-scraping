import scrapy
from bs4 import BeautifulSoup
from lab2.items import SanatoryItem, DescriptionItem


class ZakarpatSpider(scrapy.Spider):
    name = "zakarpat"
    allowed_domains = ["zakarpattyachko.com.ua"]
    start_urls = ["https://zakarpattyachko.com.ua"]

    def parse(self, response):
        soup = BeautifulSoup(response.body,  "html.parser")
        
        fac_list = soup.find(class_="nav nav-list footer-list")
        for li in fac_list.find_all("li"):
            a = li.find("a")
            
            sanat_name = a.find(string=True, recursive=False)
            sanat_url = f"https://zakarpattyachko.com.ua{a.get('href')}"
            yield SanatoryItem(
                name=sanat_name,
                url=sanat_url
            )
            yield scrapy.Request(
                # адреса сторінки, яку необхідно парсити
                url=sanat_url,
                # метод для обробки результатів завантаження
                callback=self.parse_sanat,
                # передаємо дані про факультет в функцію колбеку
                meta={
                    "faculty": sanat_name
                }
            )
    def parse_faculty(self, response):
        soup = BeautifulSoup(response.body,  "html.parser")
        dep_list = soup.find(class_="departments")
        
        if dep_list:
            for li in dep_list.find_all("li"):
                
                dep_name = li.a.find(string=True, recursive=False)
                
                dep_url = f"https://uzhnu.edu.ua{li.a.get('href')}"
                
                yield DescriptionItemItem(
                    name=dep_name,
                    url=dep_url,
                    # факультет дізнаємось із метаданих, переданих при запиті
                    faculty=response.meta.get("faculty")
                )
    
        
