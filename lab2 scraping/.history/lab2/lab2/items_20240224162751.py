# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class SanatoryItem(scrapy.Item):
    name = scrapy.Field()
    url = scrapy.Field()

class OpItem(scrapy.Item):
    name = scrapy.Field()
    department = scrapy.Field()